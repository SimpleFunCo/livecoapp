package com.gmail.simplefunco.livecoapp.Controller;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gmail.simplefunco.livecoapp.Model.ItemBidPojo;
import com.gmail.simplefunco.livecoapp.Model.ItemMyPojo;
import com.gmail.simplefunco.livecoapp.R;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

/* Created on 7/5/2017. */

public class ExpandableAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> mListHeaders;
    // private HashMap<String, List<String>> mListHashMap;
    private HashMap<String, List<ItemMyPojo>> mHashMapMyPojos;
    private HashMap<String, List<ItemBidPojo>> mHashMapBidPojos;

    public ExpandableAdapter(Context context, List<String> listHeaders,
                             HashMap<String, List<ItemMyPojo>> hashMapMyPojos,
                             HashMap<String, List<ItemBidPojo>> hashMapBidPojos) {
        mContext = context;
        mListHeaders = listHeaders;
        mHashMapMyPojos = hashMapMyPojos;
        mHashMapBidPojos = hashMapBidPojos;
    }

    @Override
    public int getGroupCount() {
        return mListHeaders.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        switch (groupPosition) {
            case 0:
                return mHashMapMyPojos.get(mListHeaders.get(groupPosition)).size();
            case 1:
                return mHashMapBidPojos.get(mListHeaders.get(groupPosition)).size();
            default:
                return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mListHeaders.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        switch (groupPosition) {
            case 0:
                return mHashMapMyPojos.get(mListHeaders.get(groupPosition)).get(childPosition);
            case 1:
                return mHashMapBidPojos.get(mListHeaders.get(groupPosition)).get(childPosition);
            default:
                return 0;
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        String groupTitle = (String) getGroup(groupPosition);

        switch (groupPosition) {
            case 0:
                // if (convertView == null) {
                LayoutInflater inflaterMyMoney = (LayoutInflater)
                        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflaterMyMoney.inflate(R.layout.expandable_list_group_my, null);

                LinearLayout layoutMyMoneyBackground = (LinearLayout)
                        convertView.findViewById(R.id.expandable_item_myMoneyBackground);
                ImageView imageGroupMyMoneyArrow = (ImageView)
                        convertView.findViewById(R.id.group_expandable_myMoneyImageArrow);

                if (isExpanded) {
                    layoutMyMoneyBackground.setBackgroundColor(
                            ContextCompat.getColor(mContext,
                            R.color.menu_my_currency_opened)
                    );
                    imageGroupMyMoneyArrow.setImageDrawable(
                            ContextCompat.getDrawable(mContext,
                            R.drawable.ic_arrow_up_drop_circle_outline_white_36dp)
                    );
                } else {
                    layoutMyMoneyBackground.setBackgroundColor(
                            ContextCompat.getColor(mContext,
                                    R.color.menu_my_currency_closed)
                    );
                    imageGroupMyMoneyArrow.setImageDrawable(
                            ContextCompat.getDrawable(mContext,
                                    R.drawable.ic_arrow_down_drop_circle_outline_white_36dp)
                    );
                }
                // }
                break;
            case 1:
                // if (convertView == null) {
                LayoutInflater inflaterBid = (LayoutInflater)
                        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflaterBid.inflate(R.layout.expandable_list_group_bid, null);

                LinearLayout layoutBidBackground = (LinearLayout)
                        convertView.findViewById(R.id.expandable_item_bidBackground);
                ImageView imageGroupBidArrow = (ImageView)
                        convertView.findViewById(R.id.group_expandable_bidImageArrow);

                if (isExpanded) {
                    layoutBidBackground.setBackgroundColor(
                            ContextCompat.getColor(mContext,
                                    R.color.menu_best_offers_opened)
                    );
                    imageGroupBidArrow.setImageDrawable(
                            ContextCompat.getDrawable(mContext,
                                    R.drawable.ic_arrow_up_drop_circle_outline_white_36dp)
                    );
                } else {
                    layoutBidBackground.setBackgroundColor(
                            ContextCompat.getColor(mContext,
                                    R.color.menu_best_offers_closed)
                    );
                    imageGroupBidArrow.setImageDrawable(
                            ContextCompat.getDrawable(mContext,
                                    R.drawable.ic_arrow_down_drop_circle_outline_white_36dp)
                    );
                }
                // }
                break;
            default:
                // if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)
                        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.expandable_list_group_my, null);
                // }
                break;
        }

        TextView listHeader = (TextView) convertView.findViewById(R.id.group_expandable_text);
        listHeader.setText(groupTitle);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {

        switch (groupPosition) {
            case 0:
                // if (convertView == null) {
                ItemMyPojo myMoney = (ItemMyPojo) getChild(groupPosition, childPosition);

                LayoutInflater inflaterMyMoney = (LayoutInflater)
                        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflaterMyMoney.inflate(R.layout.expandable_list_item_my, null);

                ImageView imageCurrencyIcon = (ImageView)
                        convertView.findViewById(R.id.expandable_item_currencyIcon);
                TextView textCurrencyAmount = (TextView)
                        convertView.findViewById(R.id.expandable_item_currencyAmount);
                TextView textCurrencyName = (TextView)
                        convertView.findViewById(R.id.expandable_item_currencyName);
                TextView textCurrencyShortName = (TextView)
                        convertView.findViewById(R.id.expandable_item_currencyShortName);
                TextView textCurrencyBid = (TextView)
                        convertView.findViewById(R.id.expandable_item_currencyBid);
                TextView textBidShortName = (TextView)
                        convertView.findViewById(R.id.expandable_item_bidShortName);
                TextView textBidToOne = (TextView)
                        convertView.findViewById(R.id.expandable_item_bidToOne);
                View viewMyMoneyDivider = (View)
                        convertView.findViewById(R.id.expandable_item_myMoneyDivider);

                imageCurrencyIcon.setImageDrawable(myMoney.getCurrencyIcon());
                textCurrencyName.setText(myMoney.getCurrencyName());
                if (myMoney.getCurrencyShortName().equals("BTC")) {                 // workaround for second color
                    textCurrencyName.setTextColor(                                  // best way is to check the image's
                            ContextCompat.getColor(mContext, R.color.btc_color));   // bitmap and get it's main color
                }
                // BigDecimal removeExponentMyMoney = BigDecimal.valueOf(myMoney.getCurrencyAmount());
                DecimalFormat formatter = new DecimalFormat("####,######");
                String output = formatter.format(myMoney.getCurrencyAmount());
                // textCurrencyAmount.setText(removeExponentMyMoney.toPlainString());
                textCurrencyAmount.setText(output);
                textCurrencyShortName.setText(myMoney.getCurrencyShortName());
                BigDecimal removeExponentMyMoney = BigDecimal.valueOf(myMoney.getCurrencyBid());
                formatter = new DecimalFormat("####.####");
                String formattedCurrencyBid = formatter.format(removeExponentMyMoney);
                StringBuilder currencyBidAddSpace = new StringBuilder(formattedCurrencyBid);
                formattedCurrencyBid = mContext.getString(R.string.myMoney_bid,
                        new StringBuilder(currencyBidAddSpace.insert(1, ' ')).toString());
                // textCurrencyBid.setText(removeExponentMyMoney.toPlainString());

                textCurrencyBid.setText(formattedCurrencyBid);
                textBidShortName.setText(myMoney.getBidName());
                removeExponentMyMoney = BigDecimal.valueOf(myMoney.getBidToOne());
                // DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols.getInstance();
                // otherSymbols.setDecimalSeparator(',');
                // formatter = new DecimalFormat("#,###");
                String bidToOne = mContext.getString(R.string.myMoney_bidToOne,
                        myMoney.getCurrencyShortName(),
                        removeExponentMyMoney.toPlainString().replace(".", ","));
                textBidToOne.setText(bidToOne);

                if (isLastChild) viewMyMoneyDivider.setVisibility(View.INVISIBLE);
                // }
                break;
            case 1:
                // if (convertView == null) {
                ItemBidPojo bid = (ItemBidPojo) getChild(groupPosition, childPosition);

                LayoutInflater inflaterBid = (LayoutInflater)
                        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflaterBid.inflate(R.layout.expandable_list_item_bid, null);

                TextView textBidHeader = (TextView)
                        convertView.findViewById(R.id.expandable_item_bidHeader);
                ImageView imageBidIconFirst = (ImageView)
                        convertView.findViewById(R.id.expandable_item_bidIconFirst);
                ImageView imageBidIconSecond = (ImageView)
                        convertView.findViewById(R.id.expandable_item_bidIconSecond);
                TextView textBidNameFirst = (TextView)
                        convertView.findViewById(R.id.expandable_item_bidNameFirst);
                TextView textBidNameSecond = (TextView)
                        convertView.findViewById(R.id.expandable_item_bidNameSecond);
                TextView textBidAmountFirst = (TextView)
                        convertView.findViewById(R.id.expandable_item_bidAmountFirst);
                TextView textBidAmountSecond = (TextView)
                        convertView.findViewById(R.id.expandable_item_bidAmountSecond);
                View viewBidDivider = (View)
                        convertView.findViewById(R.id.expandable_item_bidDivider);

                textBidHeader.setText(bid.getBidHeader());
                imageBidIconFirst.setImageDrawable(bid.getBidIconFirst());
                imageBidIconSecond.setImageDrawable(bid.getBidIconSecond());
                String stringCurrencyPair =
                        mContext.getString(R.string.bestOffers_currencyToOther,
                                bid.getBidNameFirst(), bid.getBidNameSecond());
                textBidNameFirst.setText(stringCurrencyPair);
                stringCurrencyPair =
                        mContext.getString(R.string.bestOffers_currencyToOther,
                                bid.getBidNameSecond(), bid.getBidNameFirst());
                textBidNameSecond.setText(stringCurrencyPair);
                BigDecimal removeExponentBid = BigDecimal.valueOf(bid.getBidAmountFirst());
                String stringCurrencySign = mContext.getString(R.string.bestOffers_currencySign,
                        "$", removeExponentBid.toPlainString());
                textBidAmountFirst.setText(stringCurrencySign);
                removeExponentBid = BigDecimal.valueOf(bid.getBidAmountSecond());
                textBidAmountSecond.setText(removeExponentBid.toPlainString());

                if (isLastChild) viewBidDivider.setVisibility(View.INVISIBLE);
                // }
                break;
            default:
                // if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)
                        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.expandable_list_item_my, null);
                // }
                break;
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
