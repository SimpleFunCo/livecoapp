package com.gmail.simplefunco.livecoapp.Model;
/* Created on 7/5/2017. */

import android.graphics.drawable.Drawable;

public class ItemMyPojo {

    private Drawable currencyIcon;
    private String currencyName;
    private Integer currencyAmount;
    private String currencyShortName;
    private Double currencyBid;
    private String bidName;
    private Double bidToOne;

    public Drawable getCurrencyIcon() {
        return currencyIcon;
    }

    public void setCurrencyIcon(Drawable currencyIcon) {
        this.currencyIcon = currencyIcon;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Integer getCurrencyAmount() {
        return currencyAmount;
    }

    public void setCurrencyAmount(Integer currencyAmount) {
        this.currencyAmount = currencyAmount;
    }

    public String getCurrencyShortName() {
        return currencyShortName;
    }

    public void setCurrencyShortName(String currencyShortName) {
        this.currencyShortName = currencyShortName;
    }

    public Double getCurrencyBid() {
        return currencyBid;
    }

    public void setCurrencyBid(Double currencyBid) {
        this.currencyBid = currencyBid;
    }

    public String getBidName() {
        return bidName;
    }

    public void setBidName(String bidName) {
        this.bidName = bidName;
    }

    public Double getBidToOne() {
        return bidToOne;
    }

    public void setBidToOne(Double bidToOne) {
        this.bidToOne = bidToOne;
    }
}
