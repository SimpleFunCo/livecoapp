package com.gmail.simplefunco.livecoapp.Model;
/* Created on 7/5/2017. */

import android.graphics.drawable.Drawable;

public class ItemBidPojo {

    private String bidHeader;
    private Drawable bidFirstIcon;
    private Drawable bidSecondIcon;
    private String bidFirstNameShort;
    private String bidSecondNameShort;
    private Double bidFirstAmount;
    private Double bidSecondAmount;

    public String getBidHeader() {
        return bidHeader;
    }

    public void setBidHeader(String bidHeader) {
        this.bidHeader = bidHeader;
    }

    public Drawable getBidIconFirst() {
        return bidFirstIcon;
    }

    public void setBidFirstIcon(Drawable bidFirstIcon) {
        this.bidFirstIcon = bidFirstIcon;
    }

    public Drawable getBidIconSecond() {
        return bidSecondIcon;
    }

    public void setBidSecondIcon(Drawable bidSecondIcon) {
        this.bidSecondIcon = bidSecondIcon;
    }

    public String getBidNameFirst() {
        return bidFirstNameShort;
    }

    public void setBidNameFirst(String bidFirstNameShort) {
        this.bidFirstNameShort = bidFirstNameShort;
    }

    public String getBidNameSecond() {
        return bidSecondNameShort;
    }

    public void setBidNameSecond(String bidSecondNameShort) {
        this.bidSecondNameShort = bidSecondNameShort;
    }

    public Double getBidAmountFirst() {
        return bidFirstAmount;
    }

    public void setBidAmountFirst(Double bidFirstAmount) {
        this.bidFirstAmount = bidFirstAmount;
    }

    public Double getBidAmountSecond() {
        return bidSecondAmount;
    }

    public void setBidAmountSecond(Double bidSecondAmount) {
        this.bidSecondAmount = bidSecondAmount;
    }
}
