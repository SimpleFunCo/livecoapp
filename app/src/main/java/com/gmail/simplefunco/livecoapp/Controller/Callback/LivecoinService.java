package com.gmail.simplefunco.livecoapp.Controller.Callback;
/* Created on 7/4/2017. */

import com.gmail.simplefunco.livecoapp.Model.BidHistoryPojo;
import com.gmail.simplefunco.livecoapp.Model.BidPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LivecoinService {

    @GET("exchange/ticker")
    Call<List<BidPojo>> getAllBids();

    @GET("exchange/ticker")
    Call<BidPojo> getBid(@Query("currencyPair") String currencyPair);

    @GET("exchange/order_book")
    Call<BidHistoryPojo> getBidStats(@Query("currencyPair") String currencyPair);
}
