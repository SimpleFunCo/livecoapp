package com.gmail.simplefunco.livecoapp.Ui;
/* Created on 7/6/2017. */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.gmail.simplefunco.livecoapp.R;

public class ChooseTaskActivity extends AppCompatActivity {

    private Button buttonChooseTaskOne;
    private Button buttonChooseTaskTwo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choosetask);

        buttonChooseTaskOne = (Button) findViewById(R.id.chooseTasks_buttonTaskOne);
        buttonChooseTaskTwo = (Button) findViewById(R.id.chooseTasks_buttonTaskTwo);

        buttonChooseTaskOne.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), CurrencyBidActivity.class);
            startActivity(intent);
        });

        buttonChooseTaskTwo.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), EmercoinActivity.class);
            startActivity(intent);
        });
    }
}
