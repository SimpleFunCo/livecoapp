package com.gmail.simplefunco.livecoapp.Ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.simplefunco.livecoapp.Controller.RestManager;
import com.gmail.simplefunco.livecoapp.Model.BidHistoryPojo;
import com.gmail.simplefunco.livecoapp.Model.BidPojo;
import com.gmail.simplefunco.livecoapp.R;
import com.gmail.simplefunco.livecoapp.Util.Utilities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.dacer.androidcharts.LineView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyBidActivity extends AppCompatActivity {

    private static final String TAG = "CurrencyBidActivity";
    private RestManager mRestManager;
    private ArrayList<BidPojo> mBidPojos;
    private ArrayList<String> mListCurrencies;
    private ArrayAdapter<String> mArrayAdapterFirst;
    private ArrayAdapter<String> mArrayAdapterSecond;
    private BidHistoryPojo bidHistory;

    @BindView(R.id.bid_LastInfo)
    public TextView mLastBid;
    @BindView(R.id.bid_BestInfo)
    public TextView mBestBid;
    @BindView(R.id.bid_spinnerFirstCurrency)
    public Spinner mSpinnerFirst;
    @BindView(R.id.bid_spinnerSecondCurrency)
    public Spinner mSpinnerSecond;
    @BindView(R.id.activity_currency_blockUi)
    public RelativeLayout mLayoutBlockUi;
    @BindView(R.id.activity_currency_barLoading)
    public ProgressBar mPBarLoading;
    @BindView(R.id.activity_currency_buttonRetry)
    public Button mButtonRetry;
    @BindView(R.id.activity_currency_textViewUnavailable)
    public TextView mTextViewUnavailable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_bid_loading);

        ButterKnife.bind(this);

        blockUi();

        ViewCompat.setElevation(mSpinnerFirst, 3);
        ViewCompat.setElevation(mSpinnerSecond, 3);

        mRestManager = new RestManager();

        mButtonRetry.setOnClickListener(v -> {
            mButtonRetry.setEnabled(false);

            Timer buttonTimer = new Timer();
            buttonTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    runOnUiThread(() -> mButtonRetry.setEnabled(true));
                }
            }, 7000);

            if (Utilities.isOnline(getApplicationContext())) {
                fetchAllBids();
            } else {
                Toast.makeText(getApplicationContext(), "Please, check your internet state",
                        Toast.LENGTH_SHORT).show();
            }
        });

        if (Utilities.isOnline(getApplicationContext())) {
            fetchAllBids();
        }
    }

    private void fetchAllBids() {
        Call<List<BidPojo>> listBidsCall = mRestManager.getLivecoinService().getAllBids();
        listBidsCall.enqueue(new Callback<List<BidPojo>>() {
            @Override
            public void onResponse(Call<List<BidPojo>> call, Response<List<BidPojo>> response) {
                if (response.isSuccessful()) {
                    List<BidPojo> bids = response.body();

                    mBidPojos = new ArrayList<>(bids.size());
                    mListCurrencies = new ArrayList<>();
                    mBidPojos.addAll(bids);

                    for (BidPojo b : mBidPojos) {
                        if (!mListCurrencies.contains(b.getCur())) {
                            mListCurrencies.add(b.getCur());
                        }
                    }
                    // mBidPojos.stream().filter(b ->
                    //         !mListCurrencies.contains(b.getCur()))
                    //         .forEach(b -> mListCurrencies.add(b.getCur()));

                    setUpSpinners();
                } else {
                    unBlockUi();
                    Log.d(TAG, "onResponse: FAIL " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<BidPojo>> call, Throwable t) {
                Log.d(TAG, "onResponse: FAILURE ");
            }
        });
    }

    private void blockUi() {
        mLayoutBlockUi.setVisibility(View.VISIBLE);
        mLayoutBlockUi.setClickable(true);
        mPBarLoading.setVisibility(View.VISIBLE);
        if (!Utilities.isOnline(getApplicationContext())) {
            mButtonRetry.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Please, check your internet state",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void unBlockUi() {
        mLayoutBlockUi.setVisibility(View.INVISIBLE);
        mLayoutBlockUi.setClickable(false);
        mPBarLoading.setVisibility(View.INVISIBLE);
        mButtonRetry.setVisibility(View.INVISIBLE);
    }

    private void setUpSpinners() {
        mArrayAdapterFirst =
                new ArrayAdapter<>(this, R.layout.view_spinner_item_layout, mListCurrencies);
        // mArrayAdapterFirst.setDropDownViewResource(R.layout.ms__list_item);
        // mArrayAdapterFirst.setDropDownViewResource(R.layout.view_spinner_item_layout);
        mSpinnerFirst.setAdapter(mArrayAdapterFirst);
        mArrayAdapterFirst.notifyDataSetChanged();

        mArrayAdapterSecond =
                new ArrayAdapter<>(this, R.layout.view_spinner_item_layout, findPossibleBids());
        // mArrayAdapterSecond.setDropDownViewResource(R.layout.view_spinner_item_layout);
        mSpinnerSecond.setAdapter(mArrayAdapterSecond);

        fetchSelectedBids();

        mSpinnerFirst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mTextViewUnavailable.setVisibility(View.INVISIBLE);
                blockUi();
                mArrayAdapterSecond.clear();
                mArrayAdapterSecond.addAll(findPossibleBids());
                mArrayAdapterSecond.notifyDataSetChanged();
                mSpinnerSecond.setSelection(0);
                fetchSelectedBids();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpinnerSecond.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mTextViewUnavailable.setVisibility(View.INVISIBLE);
                blockUi();
                fetchSelectedBids();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private ArrayList<String> findPossibleBids() {
        ArrayList<String> resultBids = new ArrayList<>();
        ArrayList<BidPojo> listPossibleBids =
                Utilities.indexOfAll(mSpinnerFirst.getSelectedItem().toString(), mBidPojos);

        for (BidPojo b: listPossibleBids) {
            resultBids.add(b.getSymbol() // get last 3 symbols
                    .substring(b.getSymbol().length() - 3, b.getSymbol().length()));
        }

        return resultBids;
    }

    private void fetchSelectedBids() {
            String getCurrency = mSpinnerFirst.getSelectedItem().toString() + "/" +
                    mSpinnerSecond.getSelectedItem().toString();
            Call<BidPojo> listCurrencyBid =
                    mRestManager.getLivecoinService().getBid(getCurrency);

            listCurrencyBid.enqueue(new Callback<BidPojo>() {
                @Override
                public void onResponse(Call<BidPojo> call, Response<BidPojo> response) {
                    if (response.isSuccessful()) {
                        BidPojo bidInfo = response.body();
                        // mLastBid.setText(String.valueOf(bidInfo.getLast()));
                        // mBestBid.setText(String.valueOf(bidInfo.getHigh()));
                        BigDecimal formattedLastBid = BigDecimal.valueOf(bidInfo.getLast());
                        BigDecimal formattedHighBid = BigDecimal.valueOf(bidInfo.getHigh());
                        mLastBid.setText(formattedLastBid.toPlainString());
                        mBestBid.setText(formattedHighBid.toPlainString());

                        buildChart();
                    } else {
                        unBlockUi();
                        mTextViewUnavailable.setVisibility(View.VISIBLE);
                        Log.d(TAG, "onResponse: Failed to get bidInfo. Code " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<BidPojo> call, Throwable t) {
                    Log.d(TAG, "onResponse: Failure bid info");
                }
            });
    }

    private void buildChart() {
        String getCurrency = mSpinnerFirst.getSelectedItem().toString() + "/" +
                mSpinnerSecond.getSelectedItem().toString();
        Call<BidHistoryPojo> listCurrencyBid =
                mRestManager.getLivecoinService().getBidStats(getCurrency);

        listCurrencyBid.enqueue(new Callback<BidHistoryPojo>() {
            @Override
            public void onResponse(Call<BidHistoryPojo> call, Response<BidHistoryPojo> response) {
                if (response.isSuccessful()) {
                    bidHistory = response.body();

                    LineView lineView = (LineView) findViewById(R.id.bid_currency_line_view);
                    lineView.setDrawDotLine(false);
                    lineView.setShowPopup(LineView.SHOW_POPUPS_MAXMIN_ONLY);

                    ArrayList<String> bottomText = new ArrayList<>();
                    bottomText.clear();
                    for (int i = 0; i < bidHistory.getBids().size(); i++) {
                        bottomText.add(String.valueOf(i));
                    }

                    lineView.setBottomTextList(bottomText);
                    lineView.setColorArray(new int[]{Color.BLACK,Color.GREEN,Color.GRAY,Color.CYAN});
                    ArrayList<Float> dataList = new ArrayList<>();
                    for (List<Float> d: bidHistory.getBids()) {
                        dataList.add(d.get(0));
                    }
                    ArrayList<ArrayList<Float>> generalDataList = new ArrayList<>();
                    generalDataList.add(dataList);

                    lineView.setFloatDataList(generalDataList);
                    unBlockUi();
                } else {
                    unBlockUi();
                    mTextViewUnavailable.setVisibility(View.VISIBLE);
                    Log.d(TAG, "onResponse: FAILED TO GET STATS. Code " + response.code());
                }
            }

            @Override
            public void onFailure(Call<BidHistoryPojo> call, Throwable t) {

            }
        });
    }
}
