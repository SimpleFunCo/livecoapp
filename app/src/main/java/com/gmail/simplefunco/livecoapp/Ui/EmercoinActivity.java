package com.gmail.simplefunco.livecoapp.Ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListView;

import com.gmail.simplefunco.livecoapp.Controller.ExpandableAdapter;
import com.gmail.simplefunco.livecoapp.Model.ItemBidPojo;
import com.gmail.simplefunco.livecoapp.Model.ItemMyPojo;
import com.gmail.simplefunco.livecoapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EmercoinActivity extends AppCompatActivity {

    private ExpandableListView mExpandableListView;
    private ExpandableAdapter mAdapter;
    private List<String> mHeaderList;
    private HashMap<String, List<String>> mListHashMap;
    private HashMap<String, List<ItemMyPojo>> mHashMapMyPojos;
    private HashMap<String, List<ItemBidPojo>> mHashMapBidPojos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mExpandableListView = (ExpandableListView) findViewById(R.id.activity_main_expandableView);
        initData();
        mAdapter = new ExpandableAdapter(this, mHeaderList, mHashMapMyPojos, mHashMapBidPojos);
        mExpandableListView.setAdapter(mAdapter);
    }

    private void initData() {
        mHeaderList = new ArrayList<>();
        mListHashMap = new HashMap<>();
        mHashMapMyPojos = new HashMap<>();
        mHashMapBidPojos = new HashMap<>();

        mHeaderList.add("Мои средства");
        mHeaderList.add("Лучший курс");

        List<ItemMyPojo> myMoney = new ArrayList<>();
        ItemMyPojo firstMoney = new ItemMyPojo();
        firstMoney.setCurrencyIcon(ContextCompat.getDrawable(this, R.drawable.emc));
        firstMoney.setCurrencyName("EMERCOIN");
        firstMoney.setCurrencyAmount(1000000000);
        firstMoney.setCurrencyShortName("EMC");
        firstMoney.setCurrencyBid(1400d);
        firstMoney.setBidName("USD");
        firstMoney.setBidToOne(1.4);

        ItemMyPojo secondMoney = new ItemMyPojo();
        secondMoney.setCurrencyIcon(ContextCompat.getDrawable(this, R.drawable.btc));
        secondMoney.setCurrencyName("BITCOIN");
        secondMoney.setCurrencyAmount(1);
        secondMoney.setCurrencyShortName("BTC");
        secondMoney.setCurrencyBid(1400d);
        secondMoney.setBidName("USD");
        secondMoney.setBidToOne(1.4);

        myMoney.add(firstMoney);
        myMoney.add(secondMoney);

        mHashMapMyPojos.put(mHeaderList.get(0), myMoney);

        List<ItemBidPojo> bids = new ArrayList<>();
        ItemBidPojo firstBid = new ItemBidPojo();
        firstBid.setBidHeader("Лучший спрос");
        firstBid.setBidFirstIcon(ContextCompat.getDrawable(this, R.drawable.btc));
        firstBid.setBidSecondIcon(ContextCompat.getDrawable(this, R.drawable.emc));
        firstBid.setBidNameFirst("EMC");
        firstBid.setBidNameSecond("BTC");
        firstBid.setBidAmountFirst(0.4925);
        firstBid.setBidAmountSecond(28.03);

        ItemBidPojo secondBid = new ItemBidPojo();
        secondBid.setBidHeader("Лучшее предложение");
        secondBid.setBidFirstIcon(ContextCompat.getDrawable(this, R.drawable.btc));
        secondBid.setBidSecondIcon(ContextCompat.getDrawable(this, R.drawable.emc));
        secondBid.setBidNameFirst("EMC");
        secondBid.setBidNameSecond("BTC");
        secondBid.setBidAmountFirst(0.4653);
        secondBid.setBidAmountSecond(27.03);

        bids.add(firstBid);
        bids.add(secondBid);

        mHashMapBidPojos.put(mHeaderList.get(1), bids);
    }
}
