package com.gmail.simplefunco.livecoapp.Util;
/* Created on 7/4/2017. */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.gmail.simplefunco.livecoapp.Model.BidPojo;

import java.util.ArrayList;

public class Utilities {

    private static final String TAG = "Utilities";

    public static ArrayList<BidPojo> indexOfAll(Object obj, ArrayList<BidPojo> list){
        ArrayList<BidPojo> indexList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) { // find where cur == obj
            if (obj.equals(list.get(i).getCur())) {
                indexList.add(list.get(i));
            }
        }

        return indexList;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                Log.d(TAG, "isOnline: " + activeNetwork.getTypeName());
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                Log.d(TAG, "isOnline: " + activeNetwork.getTypeName());
                return true;
            } else {
                Log.d(TAG, "isOnline: FAILUER Somehow you get here " +
                        activeNetwork.getTypeName());
                return false;
            }
        } else {
            return false;
        }
    }
}
