package com.gmail.simplefunco.livecoapp.Model;
/* Created on 7/5/2017. */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BidHistoryPojo {

    @SerializedName("bids")
    @Expose
    private List<List<Float>> bids = null;

    public List<List<Float>> getBids() {
        return bids;
    }

    public void setBids(List<List<Float>> bids) {
        this.bids = bids;
    }
}
