package com.gmail.simplefunco.livecoapp.Controller;
/* Created on 7/4/2017. */

import com.gmail.simplefunco.livecoapp.Controller.Callback.LivecoinService;
import com.gmail.simplefunco.livecoapp.Helper.Constants;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {

    private LivecoinService mLivecoinService;

    public LivecoinService getLivecoinService() {

        if (mLivecoinService == null) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .cache(null)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HTTP.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mLivecoinService = retrofit.create(LivecoinService.class);
        }

        return mLivecoinService;
    }
}
